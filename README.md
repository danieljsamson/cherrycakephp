# README #

CherryCakePHP is a distribution of the CakePHP 3 framework. It includes a series of plugins that extend the exist core of CakePHP. See wiki for details. It is in active development.

# Goal #
Is to provide plugins that give a good starting point to build your application from.

## Plugins: ##

* Customer Relationship Management
* Content Management System
* Document Management System
* Advanced Security Management
* Integration with external systems
* Graphical and Console Development Tools
* Control Panels
* Calender
* Email
* Theme & Layout hooks
* Theme Optimisation
* Upgrade & Update System.

### Details ###

* Author: Daniel Samson
* Version: 3.0
* License: MIT (Plugins may differ)

### Requires ###
* HTTP Server: Apache. mod_rewrite is required
* PHP 5.4.16 or greater. PHP-CLI
* mbstring extension
* intl extension
* Database storage engine:
    * MySQL (5.1.10 or greater)
    * PostgreSQL
    * Microsoft SQL Server (2008 or higher)
    * SQLite 3
* composer

### How do I get set up? ###

* Download CherryCake
* Open a terminal
* run composer update & composer install
* Edit config/bootstrap.php
* Edit config/app.php
* Set Permissions:

```
#!bash


HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
setfacl -R -m u:${HTTPDUSER}:rwx tmp
setfacl -R -d -m u:${HTTPDUSER}:rwx tmp
setfacl -R -m u:${HTTPDUSER}:rwx logs
setfacl -R -d -m u:${HTTPDUSER}:rwx logs
```


* Set up an apache website :

```
#!bash


<Directory />
    Options FollowSymLinks
    AllowOverride All
</Directory>
<Directory /var/www>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride All
    Order Allow,Deny
    Allow from all
</Directory>
```


### Who do I talk to? ###

* danieljsamson@gmail.com